import { ApiRequest } from "../request";

let baseUrl: string = 'http://tasque.lol/';

export class AuthController {
    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }

    async authorize(idValue: number, avatarValue: string, emailValue: string, userNameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method("POST")
        .url(`api/Register`)
        .body({
            id: idValue,
            avatar: avatarValue,
            email: emailValue,
            userName: userNameValue,
            password: passwordValue,
        })
        .send();
    return response; 
    }
    
}