import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();

describe("Get user", () => {
    it('Get all users', async () => {
        let response = await users.getAllUsers()
        console.log(response.body);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        
    });

    let accessToken: string;
    let id, userDataId, userDataToken, userDataFromId;

    before(`Login and get the token`, async () => {
        let response = await auth.login("myEmail@gmail.com", "myPassword");
        accessToken = response.body.token.accessToken.token;
        console.log(accessToken);
        userDataId = response.body;
        id = response.body.user.id;
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });

    it(`Get user from token`, async () => {
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        userDataToken = response.body;
    });

    it(`Should return correct user details by token`, async () => {
        expect(userDataId.user.id).to.equal(userDataToken.id);
        expect(userDataId.user.avatar).to.equal(userDataToken.avatar);
        expect(userDataId.user.email).to.equal(userDataToken.email);
        expect(userDataId.user.userName).to.equal(userDataToken.userName);       
    });

    it(`Get user data from Id`, async () => {
        let response = await users.getUserById(id);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        userDataFromId = response.body;
    });

    it(`Compare user details by Login and by Id`, async () => {
        expect(userDataId.user.id).to.equal(userDataFromId.id);
        expect(userDataId.user.avatar).to.equal(userDataFromId.avatar);
        expect(userDataId.user.email).to.equal(userDataFromId.email);
        expect(userDataId.user.userName).to.equal(userDataFromId.userName);       
    });

    after(`Delete user`, async () => {
        let responseDel = await users.deleteUser(id, accessToken);
        checkStatusCode(responseDel, 204);
        checkResponseTime(responseDel, 3000);
   
        let response = await users.getUserById(id);
        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);    
    });
});