import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new GetUserController();
const auth = new AuthController();

describe("Register user", () => {
    let accessToken: string;
    
    it(`Register user`, async () => {
        let response = await auth.authorize(0, "myAvatar", "myEmail@gmail.com", "Miamarim", "myPassword");
        console.log(response.body);
        accessToken = response.body.token.accessToken.token;
       expect(response.statusCode, `Status Code should be 201`).to.be.equal(201);
       checkResponseTime(response, 3000);
    });
});
