import { expect } from "chai";
import { GetUserController } from "../lib/controllers/get.user.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();

describe('Use invalid test data set for registartion', () => {
    let invalidCredentialsDataSet = [
        { id: 0, avatar: 'myAvatar', email: 'rinNew', userName: 'Meriam', password: 'myCPassword' },
        { id: 0, avatar: 'myAvatar', email: '', userName: 'Meriam', password: 'myPassword' },
        { id: 0, avatar: 'myAvatar', email: 'rinNew@gmail.com', userName: '', password: 'myPassword' },
        { id: 0, avatar: 'myAvatar', email: 'rinNew@gmail.com', userName: 'Meriam', password: '' },

    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.id}' + '${credentials.avatar}' + '${credentials.email}' + '${credentials.userName}' + '${credentials.password}'`, async () => {
            let response = await auth.authorize(credentials.id, credentials.avatar, credentials.email, credentials.userName, credentials.password);

            checkStatusCode(response, 400); 
            checkResponseTime(response, 3000);
        });
    });
});